# Running Golden Master tests

The Golden Master output is located at `src/test/golden-master`. There is a single file called `games.txt` with the entire sample of games in there. In the future, we could generate multiple samples to compare with, each in a different file, such as `1-game.txt`, `10-games.txt`, `100-games.txt` and so on.

The Golden Master test run output goes to `test-run`. **Do not track this folder in version control!** It is generated test output.

## Generate Golden Masters

```bash
$ cd $PROJECT_ROOT
$ mvn exec:java -Dexec.mainClass="ca.jbrains.games.trivia.test.GenerateGoldenMaster"
```

You can safely regenerate the Golden Master output as much as you like, because we track it in version control. If `git diff` answers "no difference", then you know that the Golden Master output is up to date. **Please remember to commit Golden Master output changes at the same time as the corresponding production code changes. Put this in the same pull request/changeset.**

## Run Golden Master Tests

```bash
$ cd $PROJECT_ROOT
$ mvn exec:java -Dexec.mainClass="ca.jbrains.games.trivia.test.CheckAgainstGoldenMaster" && diff -r test-run/ src/test/golden-master
```

If `diff` reports nothing, then the test "passes". For scripting purposes, you might prefer to check the exit code of the process: 0 is "pass" and anything else is "fail".

