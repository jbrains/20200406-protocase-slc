package com.adaptionsoft.games.uglytrivia;

public class SinglePlayerGame extends Game {
    public SinglePlayerGame(final int startingPlaceOfTheOnlyPlayer) {
        add("::only player name::");
        places[0] = startingPlaceOfTheOnlyPlayer;
    }

    public int placeOfTheOnlyPlayer() {
        return places[0];
    }
}
