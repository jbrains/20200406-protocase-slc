package ca.jbrains.games.trivia.test;

import com.adaptionsoft.games.uglytrivia.SinglePlayerGame;
import junit.framework.Assert;
import org.junit.Test;

public class RollDieTest {

    @Test
    public void happyPath() throws Exception {
        final SinglePlayerGame game = new SinglePlayerGame(3);
        game.roll(2);
        Assert.assertEquals(5, game.placeOfTheOnlyPlayer());
    }
}
