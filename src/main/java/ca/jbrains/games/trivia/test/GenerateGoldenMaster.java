package ca.jbrains.games.trivia.test;

import com.adaptionsoft.games.uglytrivia.Game;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintStream;
import java.util.Random;

public class GenerateGoldenMaster {
    private static boolean notAWinner;

    public static void main(String[] args) throws Exception {
        run(new File("src/test/golden-master"));
    }

    public static void run(final File testDataDirectory) throws FileNotFoundException {
        testDataDirectory.mkdirs();
        System.setOut(new PrintStream(new FileOutputStream(new File(testDataDirectory, "games.txt"))));

        for (int sampleSize = 0; sampleSize < 100; sampleSize++) {
            final int seed = 796 + sampleSize;

            System.out.println(String.format("--- begin game: %s ----", seed));

            Game aGame = new Game();

            aGame.add("Chet");
            aGame.add("Pat");
            aGame.add("Sue");

            Random rand = new Random(seed);

            do {
                aGame.roll(rand.nextInt(5) + 1);

                if (rand.nextInt(9) == 7) {
                    notAWinner = aGame.wrongAnswer();
                } else {
                    notAWinner = aGame.wasCorrectlyAnswered();
                }
            } while (notAWinner);
        }
    }
}
