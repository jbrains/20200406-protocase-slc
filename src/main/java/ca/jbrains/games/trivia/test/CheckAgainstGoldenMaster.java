package ca.jbrains.games.trivia.test;

import java.io.File;

public class CheckAgainstGoldenMaster {
    public static void main(String[] args) throws Exception {
        GenerateGoldenMaster.run(new File("test-run"));
    }
}
